CRISPDM alternatifleri nelerdir
Cross Industry Standard Process for Data Mining veri madenciliği için yaygın olarak kullanılan bir yöntemdir Ancak CRISPDMnin alternatifleri arasında aşağıdakiler bulunabilir
SEMMA Sample Explore Modify Model Assess yöntemi Bu yöntem SAS tarafından geliştirilmiştir ve CRISPDMye benzer şekilde veri madenciliği sürecini kapsar
KDD Knowledge Discovery in Databases yöntemi KDD veri madenciliği sürecini daha çok veri bilimine odaklar ve veri madenciliği sürecini dört temel aşamaya veri ön işleme veri keşfi veri modelleme ve sonuçların değerlendirilmesi ayırır
OSEMN Obtain Scrub Explore Model iNterpret yöntemi Bu yöntem veri madenciliği sürecini altı aşamaya böler veri elde etme veri temizliği veri keşfi veri modelleme model değerlendirmesi ve sonuçların yorumlanması
Agile Data Science yöntemi Bu yöntem veri madenciliği sürecini çok hızlı ve esnek hale getirmek için Agile yazılım geliştirme metodolojisi kullanır
Sat Jan    GMT GMT